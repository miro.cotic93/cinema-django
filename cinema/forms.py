from django.forms import ModelForm
from .models import Projection, Ticket
from django import forms
from django.db.models import Max
from django.forms import Textarea


class ProjectionForm(ModelForm):
    class Meta:
        model = Projection
        fields = ['movieTitle', 'duration', 'capacity']

class TicketForm(ModelForm):
	class Meta:
		model = Ticket
		fields = ['seatNumber', 'projection']
		labels = { 'seatNumber': 'Tickets', }
		widgets = {
			'projection': forms.HiddenInput(),
			#'seatNumber': forms.Select(attrs={'cols':80,'rows':1}),
			'seatNumber': forms.Select(attrs={'style': 'width: 100px'}),
		}


	def __init__(self,*args, **kwargs):   # disabled_choices, 
	    super(TicketForm, self).__init__(*args, **kwargs)
	    #+self.disabled_choices = disabled_choices
	    #self.fields['seatNumber'].widget.attrs['min'] = 1
	    #self.fields['seatNumber'].widget.attrs['max'] = 5
	    #self.fields['seatNumber'].disabled = True
    
	

