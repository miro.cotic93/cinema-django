from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import Projection, Ticket
from django.db.models import F
from django.db.models import Sum, Count
from django.db import models
from django.template.defaulttags import register
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormMixin
from .forms import TicketForm
from django.urls import reverse
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.messages import constants as messages
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    TemplateView,
    DeleteView
)


def about(request):
	return render(request, 'cinema/about.html')
	

def home(request):
    context = {
        'projections': Projection.objects.all()
    }
    return render(request, 'cinema/home.html', context)


class ProjectionSumView(ListView):   
	model = Ticket
	template_name = 'cinema/sum_detail.html'
	context_object_name = 'tickets'
	ordering = ['-projection'] 
	
	def get_context_data(self, **kwargs):
		context = super(ProjectionSumView, self).get_context_data(**kwargs)

		#ukupna suma nova
		counter = Ticket.objects.aggregate(total_source1=Count('projection'))
		context['total_sum2'] = sum(counter.values())

		#ukupna suma
		context['total_sum'] = Ticket.objects.all().aggregate(sum_all=Sum('seatNumber')).get('sum_all') 

		#testiranje
		context['test'] = Ticket.objects.all().values_list('projection', 'seatNumber') 

		#brojanje
		context['count_total'] = Ticket.objects.all().annotate(Count("seatNumber"))

		#brojanje
		context['count'] = Ticket.objects.values('projection').order_by('projection').annotate(Count("projection_id")).values_list('projection', 'projection_id__count')

		#pojedinacna suma
		context['commitment_sum'] = Ticket.objects.values('projection').order_by('projection').annotate(sum_all=Sum('seatNumber'))

		#mogu birat key value
		context['commitment_key'] = Ticket.objects.values('projection').order_by('projection').annotate(sum_all=Sum('seatNumber')).values_list('projection', flat=True)
		
		#stvarni key value
		context['commitment_values'] = Ticket.objects.values('projection').order_by('projection').annotate(sum_all=Sum('seatNumber')).values_list('projection', 'sum_all')	
		return context

class ProjectionTestView(ListView):   
	model = Ticket
	template_name = 'cinema/test.html'
	context_object_name = 'tickets'
	ordering = ['-projection']
	
	def get_context_data(self, **kwargs):
		context = super(ProjectionTestView, self).get_context_data(**kwargs)
	#brojanje
		context['count'] = Ticket.objects.values('projection').order_by('projection').annotate(Count("projection_id")).values_list('projection', 'projection_id__count')
		return context

		
class ProjectionListView(ListView):   #ova klasa minja cijelu funkcije home
	model = Projection
	template_name = 'cinema/home.html'
	context_object_name = 'projections'
	ordering = ['-movieDate']         #sortiranje podataka(filmova)


class ProjectionTicketView(ListView):   
	model = Ticket
	template_name = 'cinema/ticket_detail.html'
	context_object_name = 'tickets'
	ordering = ['-projection']   


class ProjectionUserView(ListView):   
	model = Ticket
	template_name = 'cinema/user_detail.html'
	context_object_name = 'tickets'
	ordering = ['-projection']	


class ProjectionDetailView(SuccessMessageMixin, FormMixin, DetailView, models.Model):
	model = Projection
	form_class = TicketForm
	success_message = "Ticket successfully purchased !"
	template_name='cinema/projection_detail.html/'
	context_object_name = 'projections'

	def get_success_url(self):
		return reverse('cinema-detail', kwargs={'pk': self.object.id})

	def get_context_data(self, **kwargs):
		context = super(ProjectionDetailView, self).get_context_data(**kwargs)
		context['form'] = TicketForm(initial={'projection': self.object })

		#iteracija kapaciteta
		context['item_list1'] = [5]
		#iteracija kapaciteta
		context['item_list2'] = [1,2,3,4,5]
		#testiranje
		context['test'] = Ticket.objects.all().values_list('projection', 'seatNumber') 
		#brojanje
		context['count'] = Ticket.objects.values('projection').order_by('projection').annotate(Count("projection_id")).values_list('projection', 'projection_id__count')
		return context

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		form = self.get_form()

		#if form.is_valid() and (form.instance.seatNumber <= Projection.objects.filter(id= self.object.id).aggregate(sum_all=Sum('capacity')).get('sum_all')):
		if form.is_valid(): #and Projection.objects.filter(id= self.object.id).aggregate(sum_all=Sum('capacity')).get('sum_all') >= 0:
			return self.form_valid(form)
		else:
			messages.warning(request, 'Sorry, this seat is no longer available!')
			return self.form_invalid(form)

	def form_valid(self, form):
		form.instance.user = self.request.user
		#Projection.objects.filter(id= self.object.id).update(capacity=F('capacity') - form.instance.seatNumber)
		Projection.objects.filter(id= self.object.id).update(capacity=F('capacity') - 1)
		form.save()
		return super(ProjectionDetailView, self).form_valid(form)
			

class ProjectionCreateView(CreateView):
	model = Projection
	fields = ['movieTitle', 'duration', 'capacity']

	def form_valid(self, form):
		form.instance.user = self.request.user  #namjestanje da trenutni autor bude trenutni ulogirani user
		return super().form_valid(form)   	#overridde


class ProjectionUpdateView(UpdateView):
	model = Projection
	fields = ['capacity', 'movieTitle', 'duration', 'movieDate']

	def form_valid(self, form):
		form.instance.user = self.request.user  
		return super().form_valid(form)


class ProjectionDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Projection
    success_url = '/'

    def test_func(self):
        projection = self.get_object()
        #if self.request.user == projection.user:
        if self.request.user == self.request.user:
            return True
        return False






















#{% if request.user.is_staff %}        