# Generated by Django 3.1.4 on 2021-01-16 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cinema', '0008_auto_20210115_0033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='seatNumber',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
