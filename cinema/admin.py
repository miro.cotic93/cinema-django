from django.contrib import admin
from .models import Ticket
from .models import Projection

admin.site.register(Ticket)
admin.site.register(Projection)