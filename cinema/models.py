from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models import Sum
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError
from django.db.models import Max

from django.forms.widgets import Select
#from django.forms import widget
#from django.utils.encoding import force_unicode
from django.utils.html import escape, conditional_escape

#class SelectWithDisabled(Select):
#    def render_option(self, selected_choices, option_value, option_label):
#        option_value = force_unicode(option_value)
#        if (option_value in selected_choices):
#            selected_html = u' selected="selected"'
#        else:
#            selected_html = ''
#            disabled_html = ''
#            if isinstance(option_label, dict):
#                if dict.get(option_label, 'disabled'):
#                    disabled_html = u' disabled="disabled"'
#                    option_label = option_label['label']
#                return u'<option value="%s"%s%s>%s</option>' % (
#                    escape(option_value), selected_html, disabled_html,
#                    conditional_escape(force_unicode(option_label)))

class Projection(models.Model):
    movieTitle = models.CharField(max_length=100)
    duration = models.IntegerField(default=0)
    capacity = models.IntegerField()
    movieDate = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        #return self.title
        return '%s %s %s' % (self.movieTitle, self.capacity, self.movieDate)

    def get_absolute_url(self):
    	return reverse('cinema-detail', kwargs={'pk': self.pk})

class Ticket(models.Model):
    SEAT_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )
    seatNumber = models.CharField(max_length = 20, choices = SEAT_CHOICES, blank=True)  #, choices = SEAT_CHOICES 
    projection = models.ForeignKey(Projection, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)  

    #def Meta(self):
     #   unique_together = ("seatNumber", "projection",)


    def __str__(self):
        #return self.title
        return '%s %s %s' % (self.seatNumber, self.projection, self.user)


    def validate_unique(self, exclude=None):
        #qs = Ticket.objects.values('projection').annotate(max_value=Max('seatNumber'))
        qs = Ticket.objects.filter(projection_id=self.projection_id)
        if qs.filter(seatNumber=self.seatNumber).exists():
            raise ValidationError('error')



    def save(self, *args, **kwargs):
        #self.validate_unique()          #ne znam sta točno znači
    #    seatNumber = cal_seatNumber(self.projection)
       # seatNumber = self.seatNumber
    #    self.seatNumber = seatNumber
        self.validate_unique()
        super(Ticket, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('cinema-detail', kwargs={'pk': self.pk})

   

    class Migration:

    def forwards(self, orm):
        # Rename 'name' field to 'full_name'
        db.rename_column('first_name', 'last_name', 'role')




    def backwards(self, orm):
        # Rename 'full_name' field to 'name'
        db.rename_column('ime', 'prezime', 'uloge')