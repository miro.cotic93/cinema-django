from django.urls import path
from . import views
from . views import (
	ProjectionListView, 
	ProjectionDetailView, 
	ProjectionCreateView, 
	ProjectionUpdateView, 
	ProjectionTicketView, 
	ProjectionUserView, 
	ProjectionSumView,
    ProjectionDeleteView,
    ProjectionTestView
)


urlpatterns = [
    path('', ProjectionListView.as_view(), name='cinema-home'),  #sad zovem novu klasu umisto direk views.home, a_view sluzi za prikaz klase
    path('cinema/ticket_detail', ProjectionTicketView.as_view(), name='cinema-ticket'),
    path('cinema/test', ProjectionTestView.as_view(), name='cinema-test'),
    path('cinema/user_detail', ProjectionUserView.as_view(), name='cinema-user'),
    path('cinema/sum_detail', ProjectionSumView.as_view(), name='cinema-sum'),
    path('cinema/<int:pk>/', ProjectionDetailView.as_view(), name='cinema-detail'),  #projection_detail DEFAULT
    path('cinema/new/', ProjectionCreateView.as_view(), name='cinema-create'),
    path('cinema/<int:pk>/update', ProjectionUpdateView.as_view(), name='cinema-update'),
    path('cinema/<int:pk>/delete/', ProjectionDeleteView.as_view(), name='cinema-delete'),
    path('about/', views.about, name='cinema-about'),
]



